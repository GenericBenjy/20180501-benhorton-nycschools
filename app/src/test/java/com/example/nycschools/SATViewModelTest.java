package com.example.nycschools;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.example.nycschools.data.DataSource;
import com.example.nycschools.data.SATResponse;
import com.example.nycschools.ui.sat.SATViewModel;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SATViewModelTest {

    private SATViewModel satViewModel;
    private List<SATResponse> satResponses;
    private CompositeDisposable compositeDisposable;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private Observer<List<SATResponse>> satObserver;

    @Mock
    DataSource repository;


    @BeforeClass
    public static void setup(){
        Scheduler schedulerN = new Scheduler() {
            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run, false);
            }
        };
        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> schedulerN);
        RxJavaPlugins.setSingleSchedulerHandler(scheduler -> schedulerN);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> schedulerN);
    }

    @Before
    public void setSATViewModel(){
        MockitoAnnotations.initMocks(this);
        satViewModel = new SATViewModel(repository);
        satResponses = new ArrayList<>();
        satResponses.add(getSats());
        compositeDisposable = new CompositeDisposable();
    }
    @Test
    public void testGetSATsAndSuccess(){
        when(repository.getSatResults("03MNO1")).thenReturn(Single.just(satResponses));
        satViewModel.getSATObservable().observeForever(satObserver);
        satViewModel.getSATs("03MNO1");

        verify(satObserver).onChanged(satResponses);

    }


    public static SATResponse getSats(){
        SATResponse satResponse = new SATResponse();
        satResponse.setDbn("02M260");
        satResponse.setSchoolName("Clinton School Writers & Artists");
        satResponse.setNumOfSatTestTakers("23");
        satResponse.setSatCriticalReadingAvgScore("297");
        satResponse.setSatMathAvgScore("458");
        satResponse.setSatWritingAvgScore("397");

        return satResponse;
    }
}
