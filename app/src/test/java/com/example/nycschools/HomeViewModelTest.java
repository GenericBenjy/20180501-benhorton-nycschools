package com.example.nycschools;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.example.nycschools.data.DataSource;
import com.example.nycschools.data.NYCSchoolsResponse;
import com.example.nycschools.ui.home.HomeViewModel;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;


public class HomeViewModelTest {

    private HomeViewModel homeViewModel;
    private List<NYCSchoolsResponse> schoolsResponses;
    private CompositeDisposable compositeDisposable;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private Observer<List<NYCSchoolsResponse>> schoolObserver;

    @Mock
    DataSource repository;


@BeforeClass
    public static void setup(){
    Scheduler schedulerN = new Scheduler() {
        @Override
        public Worker createWorker() {
            return new ExecutorScheduler.ExecutorWorker(Runnable::run, false);
        }
    };
    RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> schedulerN);
    RxJavaPlugins.setSingleSchedulerHandler(scheduler -> schedulerN);
    RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> schedulerN);
}

@Before
public void setHomeViewModel(){
    MockitoAnnotations.initMocks(this);
    homeViewModel = new HomeViewModel(repository);
    schoolsResponses = new ArrayList<>();
    schoolsResponses.add(getSchool());
    compositeDisposable = new CompositeDisposable();
}
@Test
    public void testGetSchoolsAndSuccess(){
    when(repository.getSchoolResults()).thenReturn(Single.just(schoolsResponses));
    homeViewModel.getSchoolObservable().observeForever(schoolObserver);
    homeViewModel.getSchools();

    verify(schoolObserver).onChanged(schoolsResponses);

}


public static NYCSchoolsResponse getSchool(){
    NYCSchoolsResponse nycSchoolsResponse = new NYCSchoolsResponse();
    nycSchoolsResponse.setDbn("02M260");
    nycSchoolsResponse.setSchoolName("Clinton School Writers & Artists");

    return nycSchoolsResponse;
}

}
