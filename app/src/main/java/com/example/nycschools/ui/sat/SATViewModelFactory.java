package com.example.nycschools.ui.sat;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.nycschools.data.DataSource;

public class SATViewModelFactory implements ViewModelProvider.Factory {
    private final DataSource satRepository;

    public SATViewModelFactory(DataSource satRepository) {
        this.satRepository = satRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SATViewModel.class)) {
            return (T) new SATViewModel(satRepository);
        }
        throw new IllegalArgumentException("modelClass has to be of type "
                + SATViewModel.class.getSimpleName());
    }
}
