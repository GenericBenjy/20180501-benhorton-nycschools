package com.example.nycschools.ui.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import com.example.nycschools.Constants;
import com.example.nycschools.MyApplication;
import com.example.nycschools.data.NYCSchoolsResponse;
import com.example.nycschools.R;
import com.example.nycschools.ui.home.di.DaggerHomeComponent;
import com.example.nycschools.ui.home.di.HomeModule;
import com.example.nycschools.ui.sat.SATDetailsActivity;
import com.example.nycschools.databinding.ActivityMainBinding;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements OnSchoolSelectedListener{

    @Inject
    HomeViewModel homeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_main);
        DaggerHomeComponent.builder()
                .appComponent(((MyApplication)getApplication()).getAppComponent())
                .homeModule(new HomeModule(this))
                .build()
                .inject(this);


        //binding the progress loading circle to the viewmodel so it knows when to appear/disappear
        binding.setProgressVisibility(homeViewModel.getProgressObservable());
        //adapter for recyclerview is created with a listener to this activity
        SchoolAdapter schoolAdapter = new SchoolAdapter(this);
        binding.rvResults.setLayoutManager(new LinearLayoutManager(this));
        //binding the recycleview to adapter
        binding.rvResults.setAdapter(schoolAdapter);

        //observing to see when there is data
        homeViewModel.getSchoolObservable().observe(this, schoolAdapter::setData);
        //gets the results to be displayed
        homeViewModel.getSchools();
    }

    @Override
    public void onSchoolSelected(NYCSchoolsResponse nycSchoolsResponse) {
        //When a school is selected, intent to carry the school dbn is created
        Intent intent = new Intent(this, SATDetailsActivity.class);
        //checks to make sure there is dbn value
        if(nycSchoolsResponse.getDbn()!= null) {
            intent.putExtra(Constants.KEY_DBN, nycSchoolsResponse.getDbn());
            startActivity(intent);
        }else{
            //if no bdn, toast shows with error message
            Toast.makeText(getApplicationContext(),"No DBN available",Toast.LENGTH_LONG).show();
        }

    }
}
