package com.example.nycschools.ui.home.di;

import com.example.nycschools.di.AppComponent;
import com.example.nycschools.ui.home.MainActivity;

import dagger.Component;

@Component(modules = HomeModule.class, dependencies = AppComponent.class)
@HomeScope
public interface HomeComponent {
    void inject(MainActivity mainActivity);
}
