package com.example.nycschools.ui.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.widget.Toast;

import com.example.nycschools.data.DataSource;
import com.example.nycschools.data.NYCSchoolsResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends ViewModel {
    private final ObservableBoolean progressObservable;

    private final MutableLiveData<List<NYCSchoolsResponse>> schoolObservable;

    private final DataSource schoolRepository;

    private final CompositeDisposable compositeDisposable;

    public HomeViewModel(DataSource schoolRepository) {
        this.schoolRepository = schoolRepository;
        schoolObservable = new MutableLiveData<>();
        progressObservable = new ObservableBoolean();
        compositeDisposable = new CompositeDisposable();
    }

    public ObservableBoolean getProgressObservable() {
        return progressObservable;
    }

    public LiveData<List<NYCSchoolsResponse>>getSchoolObservable(){
        return schoolObservable;
    }

    public void getSchools(){
        compositeDisposable.add(schoolRepository.getSchoolResults()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
                //when is subscribes it sets the progressObservable to true which displays the loading circle
        .doOnSubscribe(disposable -> progressObservable.set(true))
                //when the event is over(i.e results have been found) the progressObservable is set to false hiding the loading circle
        .doOnEvent((success, failure) -> progressObservable.set(false))
                .subscribe(schoolObservable::setValue,throwable ->  progressObservable.set(true)));
        //.subscribe(schoolObservable::setValue));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
