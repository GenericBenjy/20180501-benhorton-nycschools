package com.example.nycschools.ui.home.di;


import android.arch.lifecycle.ViewModelProviders;


import com.example.nycschools.di.Repository;
import com.example.nycschools.data.DataSource;
import com.example.nycschools.ui.home.HomeViewModel;
import com.example.nycschools.ui.home.HomeViewModelFactory;
import com.example.nycschools.ui.home.MainActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {
    private final MainActivity mainActivity;

    public HomeModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @HomeScope
    public HomeViewModelFactory provideHomeViewModelFactory(@Repository DataSource schoolRepository){
        return new HomeViewModelFactory(schoolRepository);
    }

    @Provides
    @HomeScope
    public HomeViewModel provideHomeViewModel(HomeViewModelFactory homeViewModelFactory){
        return ViewModelProviders.of(mainActivity, homeViewModelFactory).get(HomeViewModel.class);
    }
}
