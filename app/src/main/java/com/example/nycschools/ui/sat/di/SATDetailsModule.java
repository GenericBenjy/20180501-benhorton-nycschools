package com.example.nycschools.ui.sat.di;

import android.arch.lifecycle.ViewModelProviders;

import com.example.nycschools.data.DataSource;
import com.example.nycschools.di.Repository;
import com.example.nycschools.ui.sat.SATDetailsActivity;
import com.example.nycschools.ui.sat.SATViewModel;
import com.example.nycschools.ui.sat.SATViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class SATDetailsModule {
    private final SATDetailsActivity satDetailsActivity;

    public SATDetailsModule(SATDetailsActivity satDetailsActivity) {
        this.satDetailsActivity = satDetailsActivity;
    }

    @Provides
    @SATDetailsScope
    public SATViewModelFactory provideSATViewModelFactory(@Repository DataSource satRepository){
        return new SATViewModelFactory(satRepository);
    }

    @Provides
    @SATDetailsScope
    public SATViewModel provideSATViewModel(SATViewModelFactory satViewModelFactory){
        return ViewModelProviders.of(satDetailsActivity, satViewModelFactory).get(SATViewModel.class);
    }

}
