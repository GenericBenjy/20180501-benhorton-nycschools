package com.example.nycschools.ui.home;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.nycschools.data.DataSource;

public class HomeViewModelFactory implements ViewModelProvider.Factory {

    private final DataSource schoolRepository;

    public HomeViewModelFactory(DataSource schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(schoolRepository);
        }
        throw new IllegalArgumentException("modelClass has to be of type "
                + HomeViewModel.class.getSimpleName());

    }
}
