package com.example.nycschools.ui.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.nycschools.data.NYCSchoolsResponse;
import com.example.nycschools.databinding.ItemSchoolsBinding;

import java.util.ArrayList;
import java.util.List;
//An adapter is created to display the data from the api in a RecylerView
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>{
    private final List<NYCSchoolsResponse> schoolList = new ArrayList<>();
    private static OnSchoolSelectedListener listener = null;

    public SchoolAdapter(OnSchoolSelectedListener listener){
        SchoolAdapter.listener = listener;
    }
    public void setData(List<NYCSchoolsResponse> data){
        schoolList.clear();
        schoolList.addAll(data);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        return new SchoolViewHolder(ItemSchoolsBinding.inflate(layoutInflater,viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder schoolViewHolder, int pos) {
        schoolViewHolder.bind(schoolList.get(pos));
    }

    @Override
    public int getItemCount() {
        return schoolList.size();
    }

    public static class SchoolViewHolder extends RecyclerView.ViewHolder{
        private final ItemSchoolsBinding itemSchoolsBinding;

        public SchoolViewHolder(ItemSchoolsBinding itemSchoolsBinding) {
            super(itemSchoolsBinding.getRoot());
            this.itemSchoolsBinding = itemSchoolsBinding;
        }

        void bind(NYCSchoolsResponse nycSchoolsResponse){
            //binds the data from the response to the views in the layout
            itemSchoolsBinding.setSchools(nycSchoolsResponse);
            //if there is no listener then listener is set
            if(listener!= null){
                itemSchoolsBinding.setListener(listener);
            }
            itemSchoolsBinding.executePendingBindings();
        }
    }
}
