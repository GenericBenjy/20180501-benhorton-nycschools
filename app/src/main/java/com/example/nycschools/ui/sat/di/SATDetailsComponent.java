package com.example.nycschools.ui.sat.di;

import com.example.nycschools.di.AppComponent;
import com.example.nycschools.ui.sat.SATDetailsActivity;

import dagger.Component;

@Component(modules = SATDetailsModule.class,dependencies = AppComponent.class)

@SATDetailsScope
public interface SATDetailsComponent {
    void inject(SATDetailsActivity satDetailsActivity);
}
