package com.example.nycschools.ui.home;

import com.example.nycschools.data.NYCSchoolsResponse;

public interface OnSchoolSelectedListener {
    //an interface to listen out for when a school is clicked
    void onSchoolSelected(NYCSchoolsResponse nycSchoolsResponse);
}
