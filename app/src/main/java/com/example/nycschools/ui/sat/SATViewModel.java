package com.example.nycschools.ui.sat;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;

import com.example.nycschools.data.DataSource;
import com.example.nycschools.data.SATResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SATViewModel extends ViewModel {

    private final MutableLiveData<List<SATResponse>> satObservable;

    private final DataSource satRepository;

    private final CompositeDisposable compositeDisposable;


    public SATViewModel(DataSource schoolRepository) {
        this.satRepository = schoolRepository;
        satObservable = new MutableLiveData<>();
        compositeDisposable = new CompositeDisposable();
    }

    public LiveData<List<SATResponse>> getSATObservable(){
        return satObservable;
    }


    public void getSATs(String DBN){

            compositeDisposable.add(satRepository.getSatResults(DBN)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(satObservable::setValue, Throwable::printStackTrace));

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
