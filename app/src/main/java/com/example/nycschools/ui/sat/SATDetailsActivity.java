package com.example.nycschools.ui.sat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nycschools.Constants;
import com.example.nycschools.MyApplication;
import com.example.nycschools.R;
import com.example.nycschools.data.SATResponse;
import com.example.nycschools.databinding.ActivitySatDetailsBinding;
import com.example.nycschools.ui.sat.di.DaggerSATDetailsComponent;
import com.example.nycschools.ui.sat.di.SATDetailsModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SATDetailsActivity extends AppCompatActivity {

    @Inject
    SATViewModel satViewModel;


    TextView tvName;
    TextView tvMath;
    TextView tvWriting;
    TextView tvReading;
    TextView tvTesters;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sat_details);
        DaggerSATDetailsComponent.builder()
                .appComponent(((MyApplication)getApplication()).getAppComponent())
                .sATDetailsModule(new SATDetailsModule(this))
                .build()
                .inject(this);
        //gets dbn from intent
        String dbn = getIntent().getStringExtra(Constants.KEY_DBN);
        //dbn is passed to viewmodel via getStats function
        satViewModel.getSATs(dbn);
        //observable of the results from the api call that is passed to the setdata function
        satViewModel.getSATObservable().observe(this,this::setData);

    }

    void setData(List<SATResponse> data){
        tvName=findViewById(R.id.tvSchoolName);
        tvMath=findViewById(R.id.tvMathResult);
        tvReading=findViewById(R.id.tvReadingResult);
        tvWriting=findViewById(R.id.tvWritingResult);
        tvTesters= findViewById(R.id.tvSATtakers);
        //Some schools didnt have data so a check to make sure the data isnt empty is done
        if (!data.isEmpty()) {
            String sName = data.get(0).getSchoolName();
            String rMath = data.get(0).getSatMathAvgScore();
            String rReading = data.get(0).getSatCriticalReadingAvgScore();
            String rWriting = data.get(0).getSatWritingAvgScore();
            String rTesters = data.get(0).getNumOfSatTestTakers();
            tvName.setText(sName);
            tvMath.setText(rMath);
            tvReading.setText(rReading);
            tvWriting.setText(rWriting);
            tvTesters.setText(rTesters);
        }else{
            //As there is no data a Toast is made to produce a pop up error message
            Toast.makeText(getApplicationContext(),"No Data available",Toast.LENGTH_LONG).show();
            //finish is called so that this activity ends and reverts back to main activity
            finish();
        }


    }


}
