package com.example.nycschools.net;

import com.example.nycschools.Constants;
import com.example.nycschools.data.NYCSchoolsResponse;
import com.example.nycschools.data.SATResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolsService {
//api call to get list of schools
    @GET(Constants.SCHOOL_ENDPOINT)
    Single<List<NYCSchoolsResponse>>getSchools();

    //api call to get list of SAT results for specific school identified via dbn value
    @GET(Constants.SATS_ENDPOINT)
    Single<List<SATResponse>>getSatResults(@Query("dbn")String dnn);
}
