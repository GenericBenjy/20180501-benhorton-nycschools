package com.example.nycschools;
//Constants file of values that need to be used and never changed
public class Constants {
    public static final String BASE_URL = "https://data.cityofnewyork.us/resource/";
    public static final String SCHOOL_ENDPOINT = "s3k6-pzi2.json";
    public static final String SATS_ENDPOINT ="f9bf-2cp4.json";
    public static final int CACHE_SIZE = 5 * 1024 * 1024;
    public static final int API_TIMEOUT = 30;
    public static final String KEY_DBN = "DBN_ID";

}
