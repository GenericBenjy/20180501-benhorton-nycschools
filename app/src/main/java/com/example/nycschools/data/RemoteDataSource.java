package com.example.nycschools.data;

import com.example.nycschools.net.SchoolsService;

import java.util.List;

import io.reactivex.Single;

public class RemoteDataSource implements DataSource{
    private final SchoolsService schoolsService;

    public RemoteDataSource(SchoolsService schoolsService) {
        this.schoolsService = schoolsService;
    }


    @Override
    public Single<List<NYCSchoolsResponse>> getSchoolResults() {
        //returns the list of schools from the API
        return schoolsService.getSchools();
    }

    @Override
    public Single<List<SATResponse>> getSatResults(String dbn) {
        //returns sat result from schools dbn value
        return schoolsService.getSatResults(dbn);
    }
}
