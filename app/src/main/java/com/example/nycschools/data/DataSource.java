package com.example.nycschools.data;

import java.util.List;

import io.reactivex.Single;

public interface DataSource {
    Single<List<NYCSchoolsResponse>> getSchoolResults();
    Single<List<SATResponse>>getSatResults(String dbn);
}
