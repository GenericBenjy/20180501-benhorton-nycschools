package com.example.nycschools.data;

import java.util.List;


import io.reactivex.Single;

public class SchoolsRepository implements DataSource{
    private final DataSource remoteDataSource;

    public SchoolsRepository(DataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }


    @Override
    public Single<List<NYCSchoolsResponse>> getSchoolResults() {
        return remoteDataSource.getSchoolResults();
    }

    @Override
    public Single<List<SATResponse>> getSatResults(String dbn) {
        return  remoteDataSource.getSatResults(dbn);
    }
}
