package com.example.nycschools.di;

import com.example.nycschools.data.DataSource;
import com.example.nycschools.data.SchoolsRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

//Repository module for Dagger injection
@Module
public class RepositoryModule {
    @Provides
    @Singleton
    @Repository
    public DataSource provideRepository(@Remote DataSource remoteDataSource){
        return new SchoolsRepository(remoteDataSource);
    }
}
