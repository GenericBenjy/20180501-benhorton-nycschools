package com.example.nycschools.di;

import android.app.Application;

import com.example.nycschools.data.DataSource;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = { NetworkModule.class, RepositoryModule.class })
@Singleton
public interface AppComponent {

    @Repository
    DataSource repository();

    @Component.Builder
    interface Builder {
        AppComponent build();
        @BindsInstance Builder application(Application application);
    }
}