package com.example.nycschools.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

//Qualifier identifier is created to identify the repository
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface Repository {
}