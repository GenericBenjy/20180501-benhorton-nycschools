package com.example.nycschools.di;

import android.app.Application;

import com.example.nycschools.Constants;
import com.example.nycschools.data.DataSource;
import com.example.nycschools.data.RemoteDataSource;
import com.example.nycschools.net.SchoolsService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    //Network module for Dagger injection

    @Provides
    @Singleton
    public Cache provideCache(Application application){
        return new Cache(application.getCacheDir(), Constants.CACHE_SIZE);
    }

    //HttpLoggingInterceptor is created once to be used for OKHttpClient
    @Provides
    @Singleton
    public HttpLoggingInterceptor provideHttpLoggingInterceptor(){
        return  new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    //OkHTTpClient is created once to be used for Retrofit
    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Cache cache, HttpLoggingInterceptor httpLoggingInterceptor){
        return  new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(Constants.API_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(Constants.API_TIMEOUT,TimeUnit.SECONDS)
                .cache(cache)
                .build();
    }

    //Retrofit is created once for the use in the API call
    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    @Provides
    @Singleton
    public SchoolsService provideSchoolService(Retrofit retrofit){
        return retrofit.create(SchoolsService.class);
    }

    @Provides
    @Singleton
    @Remote
    public DataSource provideRemoteDataSource(SchoolsService schoolsService){
        return new RemoteDataSource(schoolsService);
    }
}
